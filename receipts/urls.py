from django.urls import path

from receipts.views import (ExpenseCategoryCreateView, ExpenseCategoryListView, ReceiptListView, AccountCreateView, AccountListView, ExpenseCategoryListView, ReceiptCreateView)

urlpatterns = [
path("", ReceiptListView.as_view(), name="home"),
path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
path("accounts/create/", AccountCreateView.as_view(), name="account_create"),
path("accounts/""", AccountListView.as_view(), name="account_list"),
path("categories/create", ExpenseCategoryCreateView.as_view(), name="category_create"),
path("categories/""", ExpenseCategoryListView.as_view(), name="category_list"),
]
