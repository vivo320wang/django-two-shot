from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Account, ExpenseCategory, Receipt
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.db.models import Count
from receipts.forms import ReceiptForm

# Account: Create View and List View


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "accounts/create.html"

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        # account is a variable that associates w/Account model for remembering purose. .own because Account model has a property called owner but under AccountCreateView there isn't

        account.save()
        return redirect("account_list")
        # redirect back to the account list path in urls.py


class AccountListView(ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user).annotate(
            number_receipt=Count("receipts")
        )


# Expense Category: Create View and List View
class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "expense_categories/create.html"

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("category_list")


class ExpenseCategoryListView(ListView):
    model = ExpenseCategory
    template_name = "expense_categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(
            owner=self.request.user
        ).annotate(number_receipt=Count("receipts"))

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["all"] = ExpenseCategory.objects.filter(
    #         owner=self.request.user
    #     ).annotate(number_receipt=Count("receipts"))

    #     return context


# Receipt: Create View and List View
class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    # fields = ["vendor", "total", "tax", "date", "category", "account"]
    form_class = ReceiptForm
    template_name = "receipts/create.html"

    def get_form_kwargs(self):
        kwargs = super(ReceiptCreateView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        form = Receipt(user=self.request.user)
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptListView(ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


# Create your views here.
