from unicodedata import category
from django.forms import ModelForm
from receipts.models import ExpenseCategory, Receipt, Account


class ReceiptForm(ModelForm):
    def __init__(self, *args, user, **kwargs):
        super(ReceiptForm, self).__init__(*args, **kwargs)
        self.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=user
        )
        self.fields["account"].queryset = Account.objects.filter(owner=user)

    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]
