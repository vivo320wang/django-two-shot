# Planning

- [x] install app expenses
- [ ] migrate beacuse it's a new project
- [ ] create superuser
- [x] install "accounts" app
- [x] add allowed host "127.0.0.1" in settings
- [x] add "accounts" app in settings INSTALL_APPS
- [x] create urls.py under "accounts" app
- [x] inside accounts/urls.py:
  - [x] import the LoginView and create path of "login/" in urlpatterns = [] with name of login
- [x] inside expenses(project directory)/urls.py:
  - [x] add "accounts" urls in the path "accounts/" using include built it (import include )
- [x] create a folder called "templates" under accounts
- [x] create a registration folder under templates folder
- [x] create login.html inside the registration folder
- [x] create post form in login.html, and add all the HTML (main 5 HTMLs) or using the template inheritance (base.html)
- [x] in settings.py, create LOGIN_REDIRECT_URL to "home"
  - [ ] need to create redirect path
- [x] create logoutview just like how loginview was created
- [x] create function view to show sing-up form and the submission
  - [x] import UserCreationFrom from the built-in auth forms
- CREATE HOME PAGE
- [x] create another app called "receipts"
- [x] install "receipts" app in INSTALL_APPS
- [x] create a template folder in the "receipts" app
- [x] create a receipts folder in the templates folder
- [x] create a list.html inside the receipts folder and enter <h1>Home</h1> now
- [x] create class-based view in receipts app called ReceiptListView -[x] pass in requiredmixin and TemplateView for now
- [x] add template_name of list.html from receipts folder under ReceiptListView class-based view
- [x] add the ReceiptListView in the receipt's app's urls.py in the urlpatterns with path of "" and name of "home"
- [] in expenses/urls.py
  - [x] include receipts' urls.py with path "receipts/"
  - [x] import RedirectView and add this code: path("", RedirectView.as_view(url=reverse_lazy("home"))), to redirect localhost to "home" path

# Feature Two

# CreateViews:

- [x] create a new account with path receipts/accounts/create/ for path view

  - [x] create a view.py and html page for account list to redirect once user submit form to create their account, use "accounts/"<this here is empty string>"", AccountListView.as_view()), name="account_list

- [x] create a new expense category with path receipts/categories/create/ for path view

  - [x] create a view.py and html page for category list to redirect once user submit form to create their category, use "categories/"<this here is empty string>"", ExpenseCategoryListView.as_view()), name="category_list"))

- [x] create a new receipt with path receipts/create/ for path view

- [x,x,x] need authenicated for each user in order to use any of the above: is_authenticated

- [x,x,x] assign current user to User property using form_valid method:
  def form_valid(self, form):
  item = form.save(commit=False)
  item.user_property = self.request.user
  item.save()
  return redirect("some_view")

- [x] redirect back to listview for that type of object

# ListViews:

- [x] need authenicated in order to use any of the above: is_authenticated

- [x] filter data of current user using:
      def get_queryset(self):
      return Model.objects.filter(user_property=self.request.user)
